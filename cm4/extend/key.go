package extend

import (
	"gitee.com/flyiot/gorpi/cm4/gpio"
)

type Key struct {
	Gpio      *gpio.GPIO
	Ready     bool
	IsWatch   bool
	onTrigger func()
}

func NewKey(pin int) *Key {
	s := Key{}
	s.onTrigger = func() {}
	s.IsWatch = false
	var err error
	s.Gpio, err = gpio.NewGPIO(pin)
	if err == nil {
		s.Ready = true
		s.Gpio.Input()

	}

	return &s
}

func (key *Key) Watch() {
	if key.IsWatch {
		return
	}
	key.IsWatch = true
	go func() {
		for {
			if v, err := key.Gpio.WaitChange(0); err == nil {
				if !v {
					key.onTrigger()
				}
			}
		}
	}()
}

func (key *Key) OnTrigger(cb func()) {
	key.onTrigger = cb
}
