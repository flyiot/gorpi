package extend

import (
	"gitee.com/flyiot/gorpi/cm4/gpio"
)

type LED struct {
	Gpio  *gpio.GPIO
	Ready bool
}

func NewLED(pin int) *LED {
	s := LED{}
	var err error
	s.Gpio, err = gpio.NewGPIO(pin)
	if err == nil {
		s.Ready = true
		s.Gpio.Output()
		s.Gpio.Low()
	}
	return &s
}

func (led *LED) On() {
	led.Gpio.High()
}
func (led *LED) Off() {
	led.Gpio.Low()
}

func (led *LED) Flash(timeout int64) {
	go func() {
		led.Gpio.High().WaitMs(timeout).Low()
	}()
}
