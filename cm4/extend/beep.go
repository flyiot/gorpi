package extend

import (
	"gitee.com/flyiot/gorpi/cm4/gpio"
)

type BEEP struct {
	Gpio  *gpio.GPIO
	Ready bool
}

func NewBEEP(pin int) *BEEP {
	s := BEEP{}
	var err error
	s.Gpio, err = gpio.NewGPIO(pin)
	if err == nil {
		s.Ready = true
		s.Gpio.Output()
		s.Gpio.High()
	}
	return &s
}

func (beep *BEEP) On() {
	beep.Gpio.Low()
}
func (beep *BEEP) Off() {
	beep.Gpio.High()
}

func (beep *BEEP) Beep(timeout int64) {
	go func() {
		beep.Gpio.Low().WaitMs(timeout).High()
	}()
}
