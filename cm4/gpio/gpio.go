package gpio

import (
	"errors"
	"io/ioutil"
	"log"
	"os"
	"strconv"
	"strings"
	"time"
)

func NewGPIO(pin int) (*GPIO, error) {
	g := GPIO{}
	return g.OpenPin(pin)
}

type GPIO struct {
	PortNo    string
	DriverDir string
	IsReady   bool
}

// 打开一个端口，输入的是序号
func (gpio *GPIO) OpenPin(pin int) (*GPIO, error) {
	if gpioNo, ok := PinMap[pin]; ok {
		return gpio.Open(gpioNo)
	}
	return nil, errors.New("gpio pin error:" + strconv.Itoa(pin))
}

// 打开一个端口，输入的是GPIO的端口号，不是位置序号
func (gpio *GPIO) Open(gpioNo string) (*GPIO, error) {
	gpio.DriverDir = "/sys/class/gpio/gpio" + gpioNo
	gpio.PortNo = gpioNo
	if !gpio.CheckIsReady() {
		err := ioutil.WriteFile("/sys/class/gpio/export", []byte(gpioNo), 0666)
		if err != nil {
			return nil, err
		}
	}
	if !gpio.CheckIsReady() {
		return nil, errors.New("gpio error:" + gpioNo)
	}

	return gpio, nil
}

// 关闭一个端口
func (gpio *GPIO) Close() error {
	err := ioutil.WriteFile("/sys/class/gpio/unexport", []byte(gpio.PortNo), 0666)
	if err != nil {
		return err
	}
	if gpio.CheckIsReady() {
		return errors.New("gpio close error:" + gpio.PortNo)
	}
	return nil
}

// 检查是否打开
func (gpio *GPIO) CheckIsReady() bool {
	_, err := os.Stat(gpio.DriverDir)
	gpio.IsReady = err == nil || os.IsExist(err)
	return gpio.IsReady
}

// 设置为输入
func (gpio *GPIO) Input() *GPIO {
	gpio.Write("direction", InValue)
	return gpio
}

// 是否是输入
func (gpio *GPIO) IsInput() bool {
	return gpio.Read("direction") == InValue
}

// 设置为输出
func (gpio *GPIO) Output() *GPIO {
	gpio.Write("direction", OutValue)
	return gpio
}

// 是否为输出
func (gpio *GPIO) IsOutput() bool {
	return gpio.Read("direction") == OutValue
}

// 毫秒级别延时
func (gpio *GPIO) WaitMs(times int64) *GPIO {
	time.Sleep(time.Duration(times) * time.Millisecond)
	return gpio
}

// 纳秒级别延时
func (gpio *GPIO) WaitNs(times int64) *GPIO {
	time.Sleep(time.Duration(times) * time.Nanosecond)
	return gpio
}

// 设置为Low
func (gpio *GPIO) Low() *GPIO {
	gpio.Write("value", LowValue)
	return gpio
}

// 是否为Low
func (gpio *GPIO) IsLow() bool {
	return gpio.Read("value") == LowValue
}

// 设置为High
func (gpio *GPIO) High() *GPIO {
	gpio.Write("value", HighValue)
	return gpio
}

// 是否为High
func (gpio *GPIO) IsHigh() bool {
	return gpio.Read("value") != LowValue
}

// 等待端口状态变化，如果超时timeout_ms设置0，永不超时，超时后返回当前是否是High
func (gpio *GPIO) WaitChange(timeout_ms int64) (bool, error) {
	start := time.Now().UnixNano()
	value := gpio.IsHigh()
	for {
		nowvalue := gpio.IsHigh()
		if nowvalue != value {
			return nowvalue, nil
		}
		gpio.WaitNs(1000)
		if timeout_ms > 0 {
			now := time.Now().UnixNano()
			if (now-start)/1e6 > timeout_ms {
				return nowvalue, errors.New("timeout")
			}
		}
	}
}

// 等待端口变为High
func (gpio *GPIO) WaitHigh(timeout_ms int64) error {
	start := time.Now().UnixNano()
	for {
		if gpio.IsHigh() {
			return nil
		}
		gpio.WaitNs(1000)
		if timeout_ms > 0 {
			now := time.Now().UnixNano()
			if (now-start)/1e6 > timeout_ms {
				return errors.New("timeout")
			}
		}
	}
}

// 等待端口变为Low
func (gpio *GPIO) WaitLow(timeout_ms int64) error {
	start := time.Now().UnixNano()
	for {
		if gpio.IsLow() {
			return nil
		}
		gpio.WaitNs(1000)
		if timeout_ms > 0 {
			now := time.Now().UnixNano()
			if (now-start)/1e6 > timeout_ms {
				return errors.New("timeout")
			}
		}
	}
}

// 直接写GPIO文件
func (gpio *GPIO) Write(f string, data string) *GPIO {
	file := gpio.DriverDir + "/" + f
	err := ioutil.WriteFile(file, []byte(data), 0666)
	if err != nil {
		log.Fatal(err)
	}
	return gpio
}

// 直接读一个GPIO文件
func (gpio *GPIO) Read(f string) string {
	file := gpio.DriverDir + "/" + f
	data, err := ioutil.ReadFile(file)
	if err != nil {
		data = []byte("")
	}
	str := string(data)
	str = strings.Replace(str, " ", "", -1)
	str = strings.Replace(str, "\r", "", -1)
	str = strings.Replace(str, "\n", "", -1)
	return str
}
