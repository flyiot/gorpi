package gpio

const (
	InValue   string = "in"
	OutValue  string = "out"
	LowValue  string = "0"
	HighValue string = "1"
)

var (
	PinMap map[int]string
)

func init() {
	PinMap = make(map[int]string)

	// 定义可用的GPIO  端口序号=>GPIO号

	PinMap[16] = "16"
	PinMap[17] = "17"
	PinMap[18] = "18"
	PinMap[19] = "19"
	PinMap[20] = "20"
	PinMap[21] = "21"
	PinMap[22] = "22"
	PinMap[23] = "23"
	PinMap[24] = "24"
	PinMap[25] = "25"
	PinMap[26] = "26"
	PinMap[27] = "27"

}
